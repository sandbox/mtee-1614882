<?php
/**
 * @file
 * Search API Statistics Entity Views definitions
 */

/**
 * Implements hook_views_default_views().
 */
function sapistats_views_default_views() {
  $exports['top-keywords'] = _sapistats_top_keywords_view();
  $exports['admin-log'] = _sapistats_admin_log_view();
  return $exports;
}

function _sapistats_admin_log_view() {
  $view = new view();
  $view->name = 'sapistats_admin_log';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sapistats';
  $view->human_name = 'Search API Statistics: Admin Log';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Search API Statistics: Admin Log';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view sapistats';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '15';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'sindex' => 'sindex',
    'type' => 'type',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'id';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  /* Field: Search API Statistics query: Search api statistics query ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'sapistats';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: Search API Statistics query: Field */
  $handler->display->display_options['fields']['field']['id'] = 'field';
  $handler->display->display_options['fields']['field']['table'] = 'sapistats';
  $handler->display->display_options['fields']['field']['field'] = 'field';
  /* Field: Search API Statistics query: Ip */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'sapistats';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  $handler->display->display_options['fields']['ip']['exclude'] = TRUE;
  /* Field: Search API Statistics query: Sid */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['separator'] = '';
  /* Field: Search API Statistics query: Sindex */
  $handler->display->display_options['fields']['sindex']['id'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sindex']['field'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sindex']['alter']['path'] = 'admin/config/search/search_api/index/[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['alt'] = '[sindex]';
  /* Field: Search API Statistics query: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'sapistats';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  /* Field: Search API Statistics query: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'sapistats';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Search API Statistics query: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'sapistats';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  /* Field: Search API Statistics query: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'sapistats';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  /* Filter criterion: Search API Statistics query: Sindex */
  $handler->display->display_options['filters']['sindex']['id'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['filters']['sindex']['field'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sindex']['expose']['operator_id'] = 'sindex_op';
  $handler->display->display_options['filters']['sindex']['expose']['label'] = 'Sindex';
  $handler->display->display_options['filters']['sindex']['expose']['operator'] = 'sindex_op';
  $handler->display->display_options['filters']['sindex']['expose']['identifier'] = 'sindex';
  /* Filter criterion: Search API Statistics query: Field */
  $handler->display->display_options['filters']['field']['id'] = 'field';
  $handler->display->display_options['filters']['field']['table'] = 'sapistats';
  $handler->display->display_options['filters']['field']['field'] = 'field';
  $handler->display->display_options['filters']['field']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field']['expose']['operator_id'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['label'] = 'Field';
  $handler->display->display_options['filters']['field']['expose']['operator'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['identifier'] = 'field';
  /* Filter criterion: Search API Statistics query: Value */
  $handler->display->display_options['filters']['value']['id'] = 'value';
  $handler->display->display_options['filters']['value']['table'] = 'sapistats';
  $handler->display->display_options['filters']['value']['field'] = 'value';
  $handler->display->display_options['filters']['value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['value']['expose']['operator_id'] = 'value_op';
  $handler->display->display_options['filters']['value']['expose']['label'] = 'Keyword / Value';
  $handler->display->display_options['filters']['value']['expose']['operator'] = 'value_op';
  $handler->display->display_options['filters']['value']['expose']['identifier'] = 'keywordvalue';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'field' => 'field',
    'ip' => 'ip',
    'sid' => 'sid',
    'sindex' => 'sindex',
    'timestamp' => 'timestamp',
    'type' => 'type',
    'uid' => 'uid',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'sid';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ip' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'admin/reports/search-api/admin-log';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Search API: Admin Log';
  $handler->display->display_options['menu']['description'] = 'Search API: Admin Log';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  return $view;
}

function _sapistats_top_keywords_view() {
  $view = new view();
  $view->name = 'search_api_statistics_top_keywords_chart';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sapistats';
  $view->human_name = 'Search API Statistics: Top Keywords Chart';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Search API Statistics Admin Log';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view sapistats';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '15';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'sindex' => 'sindex',
    'type' => 'type',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'id';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  /* Field: Search API Statistics query: Search api statistics query ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'sapistats';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: Search API Statistics query: Field */
  $handler->display->display_options['fields']['field']['id'] = 'field';
  $handler->display->display_options['fields']['field']['table'] = 'sapistats';
  $handler->display->display_options['fields']['field']['field'] = 'field';
  /* Field: Search API Statistics query: Ip */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'sapistats';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  $handler->display->display_options['fields']['ip']['exclude'] = TRUE;
  /* Field: Search API Statistics query: Sid */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['separator'] = '';
  /* Field: Search API Statistics query: Sindex */
  $handler->display->display_options['fields']['sindex']['id'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sindex']['field'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sindex']['alter']['path'] = 'admin/config/search/search_api/index/[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['alt'] = '[sindex]';
  /* Field: Search API Statistics query: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'sapistats';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  /* Field: Search API Statistics query: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'sapistats';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Search API Statistics query: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'sapistats';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  /* Field: Search API Statistics query: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'sapistats';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  /* Filter criterion: Search API Statistics query: Sindex */
  $handler->display->display_options['filters']['sindex']['id'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['filters']['sindex']['field'] = 'sindex';
  $handler->display->display_options['filters']['sindex']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sindex']['expose']['operator_id'] = 'sindex_op';
  $handler->display->display_options['filters']['sindex']['expose']['label'] = 'Sindex';
  $handler->display->display_options['filters']['sindex']['expose']['operator'] = 'sindex_op';
  $handler->display->display_options['filters']['sindex']['expose']['identifier'] = 'sindex';
  /* Filter criterion: Search API Statistics query: Field */
  $handler->display->display_options['filters']['field']['id'] = 'field';
  $handler->display->display_options['filters']['field']['table'] = 'sapistats';
  $handler->display->display_options['filters']['field']['field'] = 'field';
  $handler->display->display_options['filters']['field']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field']['expose']['operator_id'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['label'] = 'Field';
  $handler->display->display_options['filters']['field']['expose']['operator'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['identifier'] = 'field';
  /* Filter criterion: Search API Statistics query: Value */
  $handler->display->display_options['filters']['value']['id'] = 'value';
  $handler->display->display_options['filters']['value']['table'] = 'sapistats';
  $handler->display->display_options['filters']['value']['field'] = 'value';
  $handler->display->display_options['filters']['value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['value']['expose']['operator_id'] = 'value_op';
  $handler->display->display_options['filters']['value']['expose']['label'] = 'Keyword / Value';
  $handler->display->display_options['filters']['value']['expose']['operator'] = 'value_op';
  $handler->display->display_options['filters']['value']['expose']['identifier'] = 'keywordvalue';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'sindex' => 'sindex',
    'type' => 'type',
    'value' => 'value',
  );
  $handler->display->display_options['style_options']['default'] = 'id';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sindex' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['summary'] = 'Search API Queries overview';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Search API Statistics query: Search api statistics query ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'sapistats';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['group_type'] = 'count';
  $handler->display->display_options['fields']['id']['label'] = 'Count';
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Search API Statistics query: Sindex */
  $handler->display->display_options['fields']['sindex']['id'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['table'] = 'sapistats';
  $handler->display->display_options['fields']['sindex']['field'] = 'sindex';
  $handler->display->display_options['fields']['sindex']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sindex']['alter']['path'] = 'admin/config/search/search_api/index/[sindex]';
  $handler->display->display_options['fields']['sindex']['alter']['alt'] = '[sindex]';
  /* Field: Search API Statistics query: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'sapistats';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Search API Statistics query: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'sapistats';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['path'] = 'admin/reports/search-api/top-keywords';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Search API: Top Keywords';
  $handler->display->display_options['menu']['description'] = 'Search API: Top Keywords';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  return $view;
}

