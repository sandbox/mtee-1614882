<?php
/**
 * @file
 * sapistats_aggregation.features.inc
 */

/**
 * Implements hook_default_datastore_domain().
 */
function sapistats_aggregation_default_datastore_domain() {
  $items = array();
  $items['sapistats_ds_job'] = entity_import('datastore_domain', '{
    "domain" : "sapistats_ds_job",
    "label" : "SAPI Stats Job Search",
    "rdf_mapping" : []
  }');
  return $items;
}
